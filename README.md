# Pourquoi ce repo ?

Récemment, choqué et révolté par une vidéo découverte sur youtube, diffusée par "Juan Branco", je :

* Vérifie ce dont il s'agit
* et republie la vidéo, afin de la partager à mes concitoyens, cf. : https://www.youtube.com/watch?v=X3nYpXwsdKc .

48 heures après la diffusion de cette vidéo, je découvre un premier commentaire qui me calomnie, sous couvert d'anonymat (nom d'utilisateur de la première tentative présumée de calomnie, "_Libellule Jaune_").

Je publie donc ici les preuves de ce que j'affirme, tout en les sauvegardant, ainsi que l'ensemble des échanges, grâce à notre ami distribué, `git`.

À bon entendeur, `git` vous souhaite bien le Bonjour.


# Article Original par les journalistes de Radio Nova

Lien original de l'impression écran ci-dessous : 

> 
> https://nova.fr/radionova/video-quand-emmanuel-macron-prend-des-lecons-de-strategie-politique-avec-francois-ruffin
>


![la page de l'article](https://gitlab.com/references-publiees/affaire-une/raw/master/documentation/images/impr-ecran/impr_ecran_affaire_ruffin_radio_nova_2019-12-01_09-38-52.png?inline=false)



## "_Libellule Jaune_" 

### Premier echange, première réponse

Je pose ci-dessous (impression écran youtube) la question (Hormis mon indignation) : 

> 
> Alors monsieur, je vous propose la première question suivante, pour ouvrir le débat public , à visage découvert, devant toute l'assemblée des "GJ" : 
> 
> À votre avis, par l'adjectif "_**surréaliste**_", que voulait exprimer **le(s) journaliste(s) de Radio Nova ayant réalisé l'enregistrement macron / ruffin** ?
> 

![youtube impressecran 1](https://gitlab.com/references-publiees/affaire-une/raw/master/documentation/images/impr-ecran/commentaire_inacceptable_libellule_jaune_Firefox_Screenshot_2019-12-01T09-30-05.551Z.png?inline=false)
